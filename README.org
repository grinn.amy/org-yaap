#+TITLE: Org Yaap

Yet another alert package for org.

The main difference for this package is that it runs synchronously in
the same emacs process, so will pick up any unsaved buffers.

This package also has the ability to show a persistent notification
for clocking in and out.

It also can alert for daily events without a time.

It also can repeat alerts for old events which have not been completed
yet.

* Installation

  First, [[https://gitlab.com/grinn.amy/org-yaap/-/releases][download the latest release]] (=org-yaap-x.y.z.el=)

  Then, =M-x package-install-file RET org-yaap-x.y.z.el RET=

  And enable it with =M-x org-yaap-mode=.

  Or with [[https://github.com/raxod502/straight.el#getting-started][straight]]:

  #+begin_src emacs-lisp
    (use-package org-yaap
      :straight (org-yaap :type git :host gitlab :repo "grinn.amy/org-yaap")
      :config
      (org-yaap-mode 1))
  #+end_src

  If you are using termux, add =allow-external-apps= to
  =~/.termux/termux.properties= to allow notification clicks to launch
  emacs.
  #+begin_src conf
      allow-external-apps = true
  #+end_src

  Then install termux-api
  #+begin_src sh
    pkg install termux-api
  #+end_src

* Usage
  By default, you will be notified for all scheduled headings
  and headings with a deadline within your agenda files. If a heading
  only includes the date, you will be notified at 9am on the day of
  the heading. If you don't mark a heading as done, you will be
  repeatedly notified every 30 minutes after the heading was due.
** Time & Date Specification
   The time and date can be specified together in either a SCHEDULED
   or DEADLINE cookie:

   #+begin_src org
     ,* Some event
       SCHEDULED: <2022-09-08 Thu 15:00>
     ,* Some deadline
       DEADLINE: <2022-09-08 Thu 08:30>
   #+end_src

   If a date is given without a time, the alert will be sent according
   to =org-yaap-daily-alert= or =ALERT_TIME= (9am by default). For
   example:

   #+begin_src org
     ,* TODO Something due on Wednesday
       DEADLINE: <2022-09-07 Wed>
   #+end_src

   Will be alerted once a day from Sep 7 onward until it is marked
   =DONE=.

   =SCHEDULED= and =DEADLINE= headings can also use the diary format
   to specify which days they should be alerted:

   #+begin_src org
     ,* Diary sexp alert
       SCHEDULED: <%%(diary-cyclic 7 9 7 2022)>
   #+end_src

   In this case the heading will be alerted every Wednesday from Sep 7
   onward. Note that overdue alerts cannot occur for diary headings,
   so marking an diary heading as =DONE= will prevent all future
   alerts.

   The time can also be specified plainly in the heading itself. For
   example:

   #+begin_src org
     ,* TODO Dentist at 3pm with Dr. Gates
       SCHEDULED: <2022-09-07 Wed>
   #+end_src

   Will be alerted at 3pm on Sep 7.

   You can turn off plain style time specifications, in which case the
   entry will be treated as a daily alert, by setting
   =org-agenda-search-headline-for-time= to nil.

   Plain style time specifications are useful for diary-style
   headings. For a complete example, consider a workout schedule from
   7:30am-8am everyday consisting of walking on Mondays, Wednesdays,
   and Fridays, stretching on Tuesdays Thursdays and Saturdays, and a
   core workout on Sundays.

   #+begin_src org
     ,* Go for a walk 7:30am-8am
       SCHEDULED: <%%(or (diary-cyclic 7 9 5 2022) (diary-cyclic 7 9 7 2022) (diary-cyclic 7 9 9 2022)>

     ,* Stretch 7:30am-8am
       SCHEDULED: <%%(or (diary-cyclic 7 9 6 2022) (diary-cyclic 7 9 8 2022) (diary-cyclic 7 9 10 2022)>

     ,* Core workout 7:30am-8am
       SCHEDULED: <%%(diary-cyclic 7 9 4 2022)>
   #+end_src

   Every day you will be alerted at 7:30am (depending on
   =org-yaap-alert-before= or =ALERT_BEFORE=) with the type of workout
   to do that day.

** Options
*** =org-yaap-daemon-idle-time=
    Default is =5= (seconds)

    This is the amount of idle time to wait after the start of each
    minute before calculating notifications. If nil, run the alerts
    immediately at the top of each minute without regard for user
    interruption.
*** =org-yaap-persistent-clock=
    Default is =nil=

    If non-nil, send a persistent notification when clocking in and
    remove it when clocking out.
*** =org-yaap-include-deadline=
    Default is =t= (true)

    Whether or not to show alerts for headings that have a deadline.
*** =org-yaap-include-scheduled=
    Default is =t= (true)

    Whether or not to show alerts for scheduled headings.
*** =org-yaap-alert-title= OR =ALERT_TITLE=
    Default is =Agenda=

    The message of each alert will be the title of the heading. The
    title will be this value. Can be set globally with
    =org-yaap-alert-title= and can be overridden for each heading like
    #+begin_src org
      ,* Custom title
        SCHEDULED: <2021-10-23 Sat 08:00>
        :PROPERTIES:
        :ALERT_TITLE: My new title
        :END:
    #+end_src
*** =org-yaap-alert-severity= OR =ALERT_SEVERITY=
    Default is =nil=

    The severity of each alert. Can be =critical=, =normal=, or
    =low=. Can be set globally with =org-yaap-alert-severity= and can
    be overridden for each heading like
    #+begin_src org
      ,* Critical alert
        SCHEDULED: <2021-10-23 Sat 08:00>
        :PROPERTIES:
        :ALERT_SEVERITY: critical
        :END:
    #+end_src
*** =org-yaap-alert-before= OR =ALERT_BEFORE=
    Default is =0= (minutes)

    Number of minutes before a heading is due for which an alert will
    be sent. If this is a list, each element represents an alert which
    will be sent. Can be set globally with =org-yaap-alert-before= and
    can be overridden for each heading like
    #+begin_src org
      ,* Alert 10 and 20 minutes before
        SCHEDULED: <2021-10-23 Sat 08:00>
        :PROPERTIES:
        :ALERT_BEFORE: 10 20
        :END:
      ,* Don't alert
        SCHEDULED: <2021-10-23 Sat 08:00>
        :PROPERTIES:
        :ALERT_BEFORE: nil
        :END:
    #+end_src
*** =org-yaap-alert-timeout= OR =ALERT_TIMEOUT=
    Default is =-1=

    Number of milliseconds that the alerts are visible for. Default =-1= lets
    the notification daemon decide. Can be set globally with
    =org-yaap-alert-timeout= and can be overridden for each heading like
    #+begin_src org
      ,* Alert will disappear after 10 seconds
        SCHEDULED: <2021-10-23 Sat 08:00>
        :PROPERTIES:
        :ALERT_TIMEOUT: 10000
        :END:
      ,* Alert will not automatically disappear
        SCHEDULED: <2021-10-23 Sat 08:00>
        :PROPERTIES:
        :ALERT_TIMEOUT: 0
        :END:
    #+end_src
*** =org-yaap-daily-alert= OR =ALERT_TIME=
    Default is =9= (am)

    If a heading's DEADLINE or SCHEDULED property does not have a time
    specified, an alert will be sent at this time. Can be set to nil
    to not see alerts for these headings, a single number which will
    be the top of the hour for which the alert will be sent, or a list
    of two numbers which will be the hour and minute, respectively, of
    when the alert will be set.

    Can be set globally with =org-yaap-daily-alert= and can be
    overridden for each heading like
    #+begin_src org
      ,* Don't alert
        SCHEDULED: <2021-10-23 Sat>
        :PROPERTIES:
        :ALERT_TIME: nil
        :END:
      ,* Alert at 7:30pm
        SCHEDULED: <2021-10-23 Sat>
        :PROPERTIES:
        :ALERT_TIME: 19 30
        :END:
    #+end_src
*** =org-yaap-todo-only=
    Default is =nil=

    Only alert headings which have a todo keyword.
*** =org-yaap-exclude-done=
    Default is =t= (true)

    Don't alert headings which have a done keyword.
*** =org-yaap-overdue-alerts= OR =ALERT_OVERDUE=
    Default is =30= (minutes)

    (requires that =org-yaap-exclude-done= is non-nil)

    Alert overdue headings at this interval, in minutes. If nil, don't
    alert overdue headings. If it is a list, each element represents
    an alert that many minutes after the heading was due. Can be set
    globally with =org-yaap-overdue-alerts= and can be overridden for
    each heading like
    #+begin_src org
      ,* Alert every five minutes after event
        SCHEDULED: <2021-10-23 Sat 08:00>
        :PROPERTIES:
        :ALERT_OVERDUE: 5
        :END:
      ,* Alert 20 and 40 minutes after event
        SCHEDULED: <2021-10-23 Sat 08:00>
        :PROPERTIES:
        :ALERT_OVERDUE: 20 40
        :END:
    #+end_src
*** =org-yaap-include-tags=
    Default is =nil=

    If =org-yaap-todo-only= is non-nil, this is a list of tags to
    include on top of headings which are in a todo state.
*** =org-yaap-exclude-tags=
    Default is =nil=

    List of tags to exclude from alerts.
*** =org-yaap-only-tags=
    Default is =nil=

    If non-nil, alerts will only be sent for headings with a tag in
    this list.
*** =org-yaap-alert-keywords=
    Default is =nil=

    If non-nil, alerts will only be sent for headings with an todo keyword in this list. For instance, if the value of the variable is ='("FUTURE")=, the behaviour will be as follows:
    #+begin_src org
    ,* FUTURE Alert will be sent
       SCHEDULED: <2021-10-23 Sat 08:00>
    ,* TODO Alert will not be sent
       SCHEDULED: <2021-10-23 Sat 08:00>
    #+end_src
** As a background process
   Look at the [[file:org-yaap-script][example script]] for a demonstration of how to run
   org-yaap in a separate Emacs instance. For example, to run your
   script as a user systemd service, create a service file at
   =~/.config/systemd/user/org-yaap.service=:

   #+begin_src conf
     [Unit]
     Description=Org Yaap

     [Service]
     ExecStart=<abolute path to org-yaap script>
     Restart=on-failure

     [Install]
     WantedBy=default.target
   #+end_src

   Launch this service with the commands:

   #+begin_src sh
     systemctl --user enable org-yaap
     systemctl --user start org-yaap
   #+end_src
* License
  GPLv3
* Development
** Setup
   Install [[https://github.com/doublep/eldev#installation][eldev]]
** Commands:
*** =eldev lint=
    Lint the =org-yaap.el= file
*** =eldev compile=
    Test whether ELC has any complaints
*** =eldev test=
    Run all tests in =tests= folder
*** =eldev package=
    Creates a dist folder with =org-yaap-<version>.el=
*** =eldev md5=
    Creates an md5 checksum against all files in the dist folder.
*** =eldev release=
    Release the next version of this package.

    Should be run from the main branch.

    This command prompts for the next version to be released and asks
    for release notes. It will update all package headers and the NEWS
    file with the newest version and release notes. It will create a
    commit with the new changes and an annotated tag.

    Push the new changes with =git push && git push --tags=. This will
    create a new release on gitlab.
